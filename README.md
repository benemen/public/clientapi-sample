# Voice for Windows ClientAPI Sample

Example JavaScript implementation of Voice for Windows ClientAPI by Enreach.

The implementation connects to a provided websocket and binds listeners to events sent by the server. It also provides the functionality to control calls by sending messages to the server. It automatically connects to the server if it is available and provides some basic testing printout and controls.


Navigate to [https://benemen.gitlab.io/public/clientapi-sample](https://benemen.gitlab.io/public/clientapi-sample/) to the sample client in action.

